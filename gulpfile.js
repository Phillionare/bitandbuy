var elixir = require('laravel-elixir');

// Override Default Settings
elixir.config.css.autoprefix.options.browsers = ['> 1%'];

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix
    	.sass([
    		'all.scss'
    	], 'public/core/all.css')
        .browserify([
            'custom.js'
    	], 'public/core/all.js')
        .copy('resources/assets/fonts', 'public/fonts');
});
