<!doctype html>
<html>
  <head>
      @include('includes.head')
  </head>
  <body>
    <div id="global">
      @include('includes.header')
      <div class="wrap_body">
        @yield('content')
      </div>
      @include('includes.footer')
    </div>
  </body>
</html>