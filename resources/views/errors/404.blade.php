@extends("layouts.default")
@section("content")
  <div class="page_title_section" style="text-align:left;min-height:205px;">
    <div class="txt_head_1">Nothing Found</div>
    <div class="txt_default">Sorry, the post you are looking for is not available.</div>
  </div>
@endsection