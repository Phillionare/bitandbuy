@extends("layouts.default")
@section("content")
  <div class="page_title_section" style="text-align:left;min-height:205px;">
    <div class="txt_head_1">Hmm, looks like we've run into a problem.</div>
    <div class="txt_default">Our bots will get busy solving it and you can click below to go safely back home. </div>
  </div>
@endsection