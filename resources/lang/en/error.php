<?php
return [
  'campaign custom min' => 'This reward only accepts :amount: baht contributions or more',
  'campaign custom nan' => 'Please enter your contribution (please use only digits)'
];
?>