<?php

namespace App\ViewModels;

class CampaignViewModel {

  public $campaign;

  function __construct( $campaignModel ) {
    $this->campaign = $campaignModel;
  }

  public function is_ended() {
    $days_left = $this->get_days_left();
    return $days_left["value"] == 0;
  }

  public function is_success() {
    return $this->get_progress_value() >= 100;
  }

  public function is_created_by_campaign_builder() {
    return $this->get_campaign_version() == 2;
  }

  private function get_campaign_version() {
    return $this->campaign->campaign_version;
  }

  public function have_event() {
    return $this->campaign->have_event;
  }

  public function is_new_payment_flow() {
    return $this->get_payment_version() == '2';
  }

  public function get_payment_version() {
    return $this->campaign->payment_version;
  }

  public function get_id() {
    return $this->campaign->ID;
  }

  public function get_trid() {
    return $this->campaign->trid;
  }

  public function get_category() {
    return $this->campaign->category;
  }

  public function get_featured_image() {
    if ( $this->is_created_by_campaign_builder() ) {
      return $this->get_image_url_from_campaign_builder( $this->campaign->url_featured_img );
    } else {
      return $this->get_image_url_from_local( $this->campaign->url_featured_img );
    }
  }

  private function get_image_url_from_local( $url = '' ) {
    return imageLoader( correctUrl( $url ) );
  }

  private function get_image_url_from_campaign_builder( $url = '' ) {
    return campaignBuilderUrl( $url );
  }

  public function get_discover_image() {
    return $this->get_featured_image() . '?auto=format&w=380';
  }

  public function get_thumbnail_image() {
    return $this->get_featured_image() . '?auto=format&h=450';
  }

  public function get_thumbnail_minor1_image() {
    if ( $this->is_created_by_campaign_builder() ) {
      return $this->get_image_url_from_campaign_builder( $this->campaign->url_featured_img_minor1 );
    } else {
      return $this->get_image_url_from_local( $this->campaign->url_featured_img_minor1 ) . '?auto=format&h=360';
    }
  }

  public function get_thumbnail_minor2_image() {
    if ( $this->is_created_by_campaign_builder() ) {
      return $this->get_image_url_from_campaign_builder( $this->campaign->url_featured_img_minor2 );
    } else {
      return $this->get_image_url_from_local( $this->campaign->url_featured_img_minor2 ) . '?auto=format&h=360';
    }
  }

  public function get_content() {
    if ( $this->is_created_by_campaign_builder() ) {
      return $this->replace_image_from_campaign_builder( $this->campaign->post_content );
    } else {
      return $this->campaign->post_content;
    }
  }

  private function replace_image_from_campaign_builder( $content ) {
    return str_replace( 'src="/files_upload', 'src="' . campaignBuilderUrl( "/files_upload" ), $content );
  }

  private function get_progress_value() {
    return $this->campaign->progress;
  }

  public function get_progress() {
    $progress = $this->get_progress_value();

    if ( $progress < 99 || 100 < $progress ) return ceil( $progress );
    return floor( $progress );
  }

  public function get_end_date() {
    $result = $this->campaign->end_date;
    return (new \DateTime($result))->format( 'd/m/Y' );
  }

  public function get_success_date() {
    $result = $this->campaign->success_date;
    return (new \DateTime($result))->format( 'd/m/Y' );
  }

  public function get_url() {
    return linkLocale( '/campaign/' . $this->campaign->post_name );
  }

  public function get_share_url() {
    return urlencode( $this->get_url() );
  }

  public function get_reward_url() {
    return $this->get_url() . '#rewards';
  }

  public function get_recap_url() {
    return $this->campaign->recap_url;
  }

  public function get_title() {
    return htmlentities( $this->campaign->post_title );
  }

  public function get_preview_title() {
    return strtoupper( $this->campaign->post_title );
  }

  public function get_share_title() {
    return urlencode( $this->campaign->post_title );
  }

  public function get_description() {
    return $this->campaign->description;
  }

  public function get_description_for_staff_picks() {
    return $this->campaign->description_for_staff_picks;
  }

  public function get_video() {
    return $this->campaign->video;
  }

  public function get_supporters() {
    return $this->campaign->supporters;
  }

  public function get_event_product_id() {
    $result = $this->campaign->event_product_id;
    return $result;
  }

  public function get_event_description() {
    return $this->campaign->event_description;
  }

  private function get_event_datetime() {
    return isset( $this->campaign->event_datetime ) ? $this->campaign->event_datetime : "";
  }

  private function get_event_date_format( $date_format = '' ) {
    return (new \DateTime( $this->get_event_datetime() ))->format( $date_format );
  }

  public function get_event_date() {
    return $this->get_event_date_format( 'd F Y' );
  }

  public function get_event_time() {
    return $this->get_event_date_format( 'h:i A' );
  }

  public function get_event_location() {
    return $this->campaign->event_location;
  }

  public function display_goal_amount() {
    $return = $this->campaign->display_goal_amount;
    return $return;
  }

  public function display_supporters() {
    $return = $this->campaign->display_supporters;
    return $return;
  }

  public function get_product_id() {
    $return = $this->campaign->product_id;
    return $return;
  }

  public function get_campaign_raised() {
    return '฿ ' . number_format( $this->campaign->campaign_raised );
  }

  public function get_campaign_goal() {
    return '฿ ' . number_format( $this->campaign->goal );
  }

  public function is_buy_button() {
    return !$this->is_new_payment_flow() || $this->is_success();
  }

  public function get_support_button() {
    return $this->is_buy_button() ? trans('base.buy') : trans('base.contribute');
  }

  public function get_days_left() {
    date_default_timezone_set('Asia/Bangkok');

    $days_unit = "";
    $date_now = new \DateTime();
    $date_end = new \DateTime( $this->campaign->end_date );
    $days_left = $date_now->diff( $date_end );

    // CAMPAIGN ENDED
    if ($days_left->invert == 1) {
      return array( 'value' => 0, 'unit' => 'DAYS LEFT' );
    }
    if ($days_left->days > 0) {
      return array( 'value' => $days_left->days, 'unit' => 'DAYS LEFT' );
    }
    if ($days_left->h > 0) {
      return array( 'value' => $days_left->h, 'unit' => 'HOURS LEFT' );
    }
    if ($days_left->i > 0) {
      return array( 'value' => $days_left->i, 'unit' => 'MINUTES LEFT' );
    }
    if ($days_left->s > 0) {
      return array( 'value' => $days_left->s, 'unit' => 'SECONDS LEFT' );
    }
    return array( 'value' => 0, 'unit' => 'DAYS LEFT' );
  }

  public function need_product_popup() {
    return $this->campaign->need_product_popup;
  }

  public function get_popup_product_id() {
    return $this->campaign->popup_product_id;
  }

}
