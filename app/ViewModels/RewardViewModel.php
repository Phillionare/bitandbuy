<?php

namespace App\ViewModels;

class RewardViewModel {

  public $reward;

  function __construct( $reward ) {
    $this->reward = $reward;
  }

  public function get_id() {
    return $this->reward->ID;
  }

  public function get_title() {
    return strtoupper( strip_tags( $this->reward->post_title ) );
  }

  public function get_event_title() {
    return strip_tags( $this->reward->post_title );
  }

  public function get_desciption() {
    return strip_tags( $this->reward->post_content, '<br><span>' );
  }

  public function get_price_sale() {
    return number_format( floor( $this->reward->price_sale ) );
  }

  public function get_price_sale_value() {
    return $this->reward->price_sale;
  }

  public function get_estimated_delivery() {
    return $this->reward->estimated_delivery;
  }

  public function get_image() {
    $image = $this->reward->reward_image_url;
    if ( !empty( $image ) ) return campaignBuilderUrl( $image );
    return "http://asiola.co.th/wp-content/themes/ken-child/images/rewards/" . $this->get_parent_sku()  . "/" . $this->get_sku() . ".jpg";
  }

  private function get_sku() {
    return $this->reward->sku;
  }

  private function get_parent_sku() {
    return $this->reward->parent_sku;
  }

  public function get_stock() {
    return $this->reward->stock;
  }

  public function is_custom_price() {
    return $this->reward->is_customizable_price == 'yes';
  }

  public function get_min_custom_price() {
    return isset( $this->reward->min_customizable_price ) ? $this->reward->min_customizable_price : $this->get_price_sale_value();
  }

  public function get_custom_price() {
    return explode( ',', $this->get_custom_price_value() );
  }

  public function get_custom_price_value() {
    return isset( $this->reward->predefined_customizable_price ) ? $this->reward->predefined_customizable_price : $this->get_min_custom_price();
  }

  public function is_show_stock() {
    return !$this->is_out_of_stock() && $this->is_limited_product();
  }

  public function is_out_of_stock() {
    return $this->get_stock() <= 0;
  }

  public function is_limited_product() {
    return $this->get_stock() < 100;
  }

  public function is_featured_product() {
    return $this->reward->is_featured == 'yes';
  }

  public function is_popular_product() {
    return $this->reward->total_sales > 0;
  }

}
