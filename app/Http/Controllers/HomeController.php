<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends Controller
{
	public function index()
	{
		$output = ['user' => \Auth::user()];
		return view('pages.home',$output);
	}

	public function logout()
	{
		
	}
}

?>
