<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('home');
// });


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['prefix' => LaravelLocalization::setLocale()], function() {
Route::group(['middleware' => ['web']], function () {

  Route::get('/', 'HomeController@index');
  // Authentication routes...
  Route::get('auth/fb_login/{auth?}', array('as'=>'facebookAuth', 'uses'=>'Auth\AuthController@getFacebookLogin'));
  Route::get('auth/login', 'Auth\AuthController@getLogin');
  Route::post('auth/login', 'Auth\AuthController@postLogin');
  Route::get('auth/logout', 'HomeController@logout');


  // Registration routes...
  Route::get('auth/register', 'Auth\AuthController@getRegister');
  Route::post('auth/register', 'Auth\AuthController@postRegister');
  });

  Route::get('profile', ['middleware' => 'auth', function() {
    // Only authenticated users may enter...
  }

]);
});
