<?php

namespace App\Models;

class Metas {
  private $metas;

  public function __construct($metas) {
    $this->metas = $metas;
  }

  public function __get($name){
    if (array_key_exists($name, $this->metas)) {
      return $this->metas[$name];
    } else {
      return '';
    }
  }

  public function __set($name, $value) {
  }

  public function __isset($name) {
    return isset($this->metas[$name]);
  }
}

class ModelFactory {
  public static function create_campaign_model( $campaign ) {
    return clone $campaign;
  }
}
