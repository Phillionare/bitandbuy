<?php
function imageLoader($img_path = null, $is_static = 0)
{
  if (!empty(env('CAMPAIGN_BUILDER_URL')) && (strpos($img_path, env('CAMPAIGN_BUILDER_URL')) !== false)) {
	  return $img_path;
  }
  if($is_static)
    return env('STATIC_IMG_PATH').$img_path;
  else
    return env('IMG_PATH').$img_path;
}

function imageSubLoader($img_path = null)
{
  return '../img/'.$img_path;
}

function linkLocale($link_url = null)
{
  return LaravelLocalization::localizeURL($link_url);
}

function baseUrl($extended_url = null)
{
  return env('BASE_URL').$extended_url;
}

function campaignBuilderUrl($extended_url = null)
{
  return env('CAMPAIGN_BUILDER_URL').$extended_url;
}

function calculate_campaign_days_left($campaign_end_date, &$days_unit) {
  date_default_timezone_set('Asia/Bangkok');

  $date_now = new DateTime();
  $date_end = new DateTime($campaign_end_date);
  $days_left = $date_now->diff($date_end);

  // CAMPAIGN ENDED
  if ($days_left->invert == 1) {
    $days_unit = 'DAYS LEFT';
    return 0;
  }
  if ($days_left->days > 0) {
    $days_unit = 'DAYS LEFT';
    return $days_left->days;
  }
  if ($days_left->h > 0) {
    $days_unit = 'HOURS LEFT';
    return $days_left->h;
  }
  if ($days_left->i > 0) {
    $days_unit = 'MINUTES LEFT';
    return $days_left->i;
  }
  if ($days_left->s > 0) {
    $days_unit = 'SECONDS LEFT';
    return $days_left->s;
  }
}

function calculate_time_passed($datetime) {
  //date_default_timezone_set('Asia/Bangkok');

  $date_now = new \DateTime();
  $date_end = new \DateTime($datetime);
  $days_left = $date_end->diff($date_now);

  if ($days_left->invert == 1) {
    $days_unit = 'DAYS LEFT';
    return  0;
  }
  if ($days_left->days > 0) {
    return $days_left->days . ' ' . trans_choice('base.day ago', $days_left->days);
  }
  if ($days_left->h > 0) {
    return $days_left->h . ' ' . trans_choice('base.hr ago', $days_left->h);
  }
  if ($days_left->i > 0) {
    return $days_left->i . ' ' . trans_choice('base.min ago', $days_left->i);
  }
  if ($days_left->s > 0) {
    return trans('base.just now');
  }
}

function correctUrl($url){
  if (strpos($url, 'localhost') !== false) {
    return str_replace('http://localhost:8888//wp-content/','',$url);
  }else if (strpos($url, 'staging2') !== false) {
    return str_replace('http://staging2.asiola.co.th/wp-content/','',$url);
  }else if (strpos($url, 'staging') !== false) {
    return str_replace('http://staging.asiola.co.th/wp-content/','',$url);
  }else{
    return str_replace('http://asiola.co.th/wp-content/','',$url);
  }
}

function wpMeta($data_array,$meta_key){
  if(!$data_array | !$meta_key){
    return false;
    break;
  }
  foreach ($data_array as $item){
    if($item->meta_key == $meta_key){
      return $item->meta_value;
      break;
    }
  }
}

function wp_current_user(){
  static $current_user;
  if (!isset($current_user)) {
    $current_user = false;
    foreach ($_COOKIE as $cookie_name => $cookie_value) {
      if (strpos($cookie_name, 'wordpress_logged_in') === 0){
        $user_login = explode('|',explode('%',$cookie_value)[0])[0];
          $sql = "SELECT
            wpt_users.*,
            firstname.meta_value first_name,
            lastname.meta_value last_name,
            fbimg.meta_value fb_img,
            user_level.meta_value user_level
            FROM wpt_users
            LEFT JOIN wpt_usermeta firstname ON firstname.user_id=wpt_users.id AND firstname.meta_key='first_name'
            LEFT JOIN wpt_usermeta lastname ON lastname.user_id=wpt_users.id AND lastname.meta_key='last_name'
            LEFT JOIN wpt_usermeta fbimg ON fbimg.user_id=wpt_users.id AND fbimg.meta_key='_wc_social_login_facebook_profile_image'
            LEFT JOIN wpt_usermeta user_level ON user_level.user_id=wpt_users.id AND user_level.meta_key='wpt_user_level'
            WHERE
            user_login=:user_login";
          $user_data = \DB::select( \DB::raw($sql), array('user_login' => $user_login) );

        $current_user = !empty($user_data) ? $user_data[0] : false;
        if ($current_user) {
          $current_user->is_admin = ($current_user->user_level=='10');
        }
        break;
      }
    }
  }
  return $current_user;
}

function wp_cart(){
  $user = wp_current_user();
  if($user){
    $user_session = \DB::select( \DB::raw("SELECT
    *
    FROM wpt_usermeta
    WHERE
    meta_key = '_woocommerce_persistent_cart' AND
    user_id = '".$user->ID."'"));

    return !empty($user_session) ? unserialize($user_session[0]->meta_value)["cart"] : false;
  }else{
    return false;
  }
}

function getSeoMeta($campaign_data = NUll){
  $seo_meta_list = array();

  $meta_locale = \LaravelLocalization::getCurrentLocale() == 'th' ? 'th_TH' : 'en_US';
  $meta_page_title = \LaravelLocalization::getCurrentLocale() == 'th' ? 'Asiola | ไอเดียดีๆ...มีได้ไม่สิ้นสุด' : 'Asiola: bring important ideas to life';
  $meta_description = \LaravelLocalization::getCurrentLocale() == 'th' ? 'Asiola แพลตฟอร์มคราวด์ฟันดิ้งแบบร่วมคัดสรรที่แรกของประเทศไทย ร่วมสนับสนุนไอเดียดีๆให้เกิดขึ้นจริง หรือจะเริ่มแคมเปญของคุณก็ทำได้ที่แพลตฟอร์มของเรา' : 'The first crowdfunding platform curated for Thailand. Support creators with innovative ideas, get involved in creative projects or launch your own.';
  $meta_keywords = \LaravelLocalization::getCurrentLocale() == 'th' ? 'คราวด์ฟันดิ้ง Asiola, Asiola ไอเดีย, สนับสนุน Asiola, ระดมทุนออนไลน์, คราวด์ฟันดิ้ง คือ, ศิลปินไทย' : 'crowdfund Asiola, crowdfunding Thailand, Asiola artists, Asiola ideas, crowdfunding how to, Thai art, Thai music, Thai fashion, Thai chefs';
  $meta_twitter_title =  $meta_page_title;
  $meta_twitter_description = $meta_description;
  $meta_image = 'https://asiola.co.th/wp-content/uploads/2015/12/asiola-blogpost.jpg';
  $meta_twitter_image = 'ttps://asiola.co.th/wp-content/uploads/2015/12/asiola-blogpost.jpg';

  $seo_meta_list['meta_url']            = 'http://'.$_SERVER['HTTP_HOST'].explode('#',explode('?', $_SERVER['REQUEST_URI'])[0])[0];
  $seo_meta_list['meta_locale']         = $campaign_data ? 'th_TH' : $meta_locale;
  $seo_meta_list['meta_page_title']     = wpMeta( $campaign_data, '_yoast_wpseo_title' ) ? wpMeta( $campaign_data, '_yoast_wpseo_title' ) : $meta_page_title;
  $seo_meta_list['meta_description']    = wpMeta( $campaign_data, '_yoast_wpseo_metadesc' ) ? wpMeta( $campaign_data, '_yoast_wpseo_metadesc' ) : $meta_description;
  $seo_meta_list['meta_keywords']       = wpMeta( $campaign_data, '_yoast_wpseo_metakeywords' ) ? wpMeta( $campaign_data, '_yoast_wpseo_metakeywords' ) : $meta_keywords;
  $seo_meta_list['meta_twitter_title']  = wpMeta( $campaign_data, '_yoast_wpseo_twitter' ) ? wpMeta( $campaign_data, '_yoast_wpseo_twitter' ) : $meta_twitter_title;
  $seo_meta_list['meta_twitter_description'] = wpMeta( $campaign_data, '_yoast_wpseo_metadesc' ) ? wpMeta( $campaign_data, '_yoast_wpseo_metadesc' ) : $meta_twitter_description;
  $seo_meta_list['meta_og_title']       = wpMeta( $campaign_data, '_yoast_wpseo_opengraph' ) ? wpMeta( $campaign_data, '_yoast_wpseo_opengraph' ) : $seo_meta_list['meta_page_title'];
  $seo_meta_list['meta_image']          = wpMeta( $campaign_data, '_yoast_wpseo_opengraph-image' ) ? wpMeta( $campaign_data, '_yoast_wpseo_opengraph-image' ) : $meta_image;
  $seo_meta_list['meta_twitter_image']  = wpMeta( $campaign_data, '_yoast_wpseo_twitter-image' ) ? wpMeta( $campaign_data, '_yoast_wpseo_twitter-image' ) : $meta_twitter_image;

  return $seo_meta_list;
}
